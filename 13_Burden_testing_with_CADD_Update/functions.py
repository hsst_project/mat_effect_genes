import pandas as pd
import sys
import scipy.stats as stats

def get_ref(value):
    """
    Get the reference allele from the ID column
    """
    # Remove the chromosome
    value = value.split(":")[1]
    # remove the alt allele
    value = value.split(">")[0]
    # TODO: Remove the POS
    digits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    return "".join([x for x in value if x not in digits])

def get_alt(value):
    """
    Get the alt allele from the ID column
    """
    return value.split(">")[1]

def write_line(value, outfile):
    """
    Write the variant to a VCF file
    """
    outfile.write(f"{value['CHROM']}\t{value['POS']}\t.\t{value['REF']}\t{value['ALT']}\t99\tPASS\t.\n")
    return f"{value['CHROM']}\t{value['POS']}\t{value['REF']}\t{value['ALT']}\n"

def write_vcf(target_df, outpath):
    """
    Write the given dataframe to a VCF file for CADD annotation
    """
    temp_vcf = target_df.loc[:,("CHROM", "POS", "ID")]
    temp_vcf["REF"] = temp_vcf['ID'].apply(get_ref)
    temp_vcf["ALT"] = temp_vcf.loc[:,'ID'].apply(get_alt)

    # Write this to VCF format
    with open(outpath, "w") as outfile:
        outfile.write("##fileformat=VCFv4.2\n")
        outfile.write("#CHROM POS ID REF ALT QUAL FILTER INFO\n")
        temp_vcf.sort_values(["CHROM", "POS"]).apply(write_line, args=(outfile,), axis=1)
    
    print(f"Wrote {temp_vcf.shape[0]} variants to VCF file {outpath}.", file=sys.stderr)

def add_cadd(caddfile, targetdf):
    # Read the CADD score file
    temp_cadd = pd.read_csv(caddfile, delimiter="\t", skiprows=1)
    temp_cadd['ID'] = temp_cadd.apply(lambda x: f"{x['#CHROM']}:{x['POS']}{x['REF']}>{x['ALT']}", axis=1)

    # Merge the full data set with the CADD score file
    temp_cadd = temp_cadd[['ID', 'PHRED']].set_index("ID")
    temp_variants = targetdf.set_index("ID")
    temp_cadd = temp_variants.join(temp_cadd).reset_index()

    # reset the CHROM column
    temp_cadd['CHROM'] = temp_cadd['CHROM'].apply(lambda x: 23 if x=="X" else x)

    # Sort the dataframe
    temp_cadd = temp_cadd.sort_values(['CHROM', 'POS'])
    
    return temp_cadd

def count_samples_with_variants(target_gene, dataframe):
    """Count how many samples in the given dataframe have at least 1 variant in the specified gene
    
    This can be modified to only count samples with variants matching particular modes of inheritence
    e.g. We can only count 
    """
    # subset the target dataframe to get only this gene
    temp_df = dataframe[dataframe.GENE == target_gene]
    # set MAT_COUNT == 2 and final == 1 to capture exclusively homozygous variants
    # set the final >= 1 to 2 (with MAT_COUNT >=1) to require at least a compound het, including hom
    # MAT_COUNT >=1 and .count == 1 should get single dominant variants
    # Currently, we are counting one or more variants
    temp_df_2 = temp_df[temp_df.MAT_COUNT >= 1].groupby("MAT_ID")[['MAT_COUNT']].count() >= 1
    
    # Return the number of samples in which the above criteria is matched
    answer = len(temp_df_2[temp_df_2.MAT_COUNT == True])
    
    # clear temp dataframes
    del(temp_df)
    del(temp_df_2)
    
    return answer


def do_burden_test(row):
    """Do a Fisher exact test on the case/control positive/negative counts"""
    cases = [row.loc['pos_cases'], row.loc['neg_cases']]
    controls = [row.loc['pos_controls'], row.loc['neg_controls']]
    # Use Scipy.stats fisher exact test
    # Order doesn't seem to matter as long as they are grouped correctly
    oddsratio, pvalue = stats.fisher_exact([cases, controls])
    
    if oddsratio < 1:
        # This will stop "protective" genes from appearing
        # They might be interesting, but they aren't relevant for now
        return 1
    return pvalue

# It might just be quicker (and easier!) to manually correct any genes flagged as not recognised by Panther
# NOPE - full reference list has 658 unrecognised IDs, so that's too many to update manually.

import requests
import time

# Check genes against HGNC database
def get_latest_symbol_from_hgnc(symbol: str) -> str:
    """
    https://wrgl-resources.herokuapp.com/API/genes-transcripts/gene-name/<symbol>
    """
    endpoint= "https://wrgl-resources.herokuapp.com/API/genes-transcripts/gene-name"
    query = f"{endpoint}/{symbol}"
    try:
        data = requests.get(query).json()
        if data['hgnc_id'] is not None:
            return (data['hgnc_id'])
        return symbol
    except:
        print(f"ERROR: Something went wrong checking HGNC for {symbol}", file=sys.stderr)
        return symbol
    
# Add chromosome to df for manahattan plot
def get_chromosome(hgnc: str) -> str:
    """
    Get the
    """
    headers = {'Accept': 'application/json'}
    endpoint = "http://rest.genenames.org/fetch/hgnc_id"
    query = f"{endpoint}/{hgnc}"
    if "HGNC" in hgnc:
        try:
            data = requests.get(query, headers=headers).json()
            location = data['response']['docs'][0]['location']
            locationp = location.split('p')
            locationq = location.split('q')
            if len(locationp) > len(locationq):
                return locationp[0]
            else:
                return locationq[0]
            return location
        except:
            print(f"ERROR: Something went wrong checking HGNC for gene {hgnc}", file=sys.stderr)
            return "None"
    else:
        print(f"ERROR: gene {hgnc} does not have an HGNC ID", file=sys.stderr)
        return None