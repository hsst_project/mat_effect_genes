Maternal effect genes
=====================

Introduction
------------

\#TODO

Processing
----------

 1. Create a BED file of genes for analysis (see 01_Mouse_to_Human)
 2. Use VEP to add additional annotations, and filter on them
 3. Any way to automatically ACMG classify? Or at least apply some of them?
 4. Run a test analysis on a single sample
 5. Run that analysis on all samples
