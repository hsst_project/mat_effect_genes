VEP Annotation
==============

VEP can be used to add additional information to a VCF file.

This folder is to develop a strategy for annotation with VEP that is useful for this analysis.

NOTE: VEP is installed in a conda environment. See CONDA_SETUP/README.md for details.
VEP plugins cannot be installed automatically - you have to run `vep_install -a p --PLUGINS CADD,Condel,LOVD` within the conda env.

\# TODO
