# Load conda and the VEP environment
module load conda/4.4.0
conda activate vep

# --dir is the path to the VEP data directory
# This has to be downloaded manually, so I've put it in the project folder
vep --cache \
--dir /scratch/imprinting/CAP_245/software \
-i test.vcf \
-o out.vcf \
--port 3337 \
--af_gnomad \
--plugin CADD \
--plugin Condel \
--vcf
