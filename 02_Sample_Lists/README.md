Sample Lists
============

### DDD_Full_Data.xlsx
 * Full details of all samples
 * Includes parental samples, HPO terms, etc.
 * See file for full details

### DDD_Full_Data.csv
 * CSV version of the front sheet of DDD_Full_Data.xlsx

### DDD_Trio_List.txt
 * Extracted from DDD_Full_Data.csv
 * Lists the proband ID, maternal ID, and paternal ID for each proband sample
 * Labels TRIO or SINGLETON depending on if family data is available.
  * 1840 total samples
  * 1299 trios
  * 541 singletons
