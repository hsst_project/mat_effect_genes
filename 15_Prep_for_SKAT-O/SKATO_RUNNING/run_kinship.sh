#!/bin/bash -e

#PBS -W umask=002
#PBS -W group_list=imprinting
#PBS -l walltime=06:00:00
#PBS -l nodes=1:ppn=1
#PBS -l mem=16GB

>&2 echo "JOB STARTED"

# If running in qsub, we need to change to the current directory
if [ -d "$PBS_O_WORKDIR" ]; then
    >&2 echo INFO: Running as qsub job...
    cd "$PBS_O_WORKDIR"
fi

# Set input and output files
#infile=TEMP.merged.vcf.gz #FINAL.variants_for_SKATO.vcf.gz
#output=MERGED_FULL

rvtests/executable/vcf2kinship --inVcf TEMP.merged.vcf.gz --bn --out KINSHIP
