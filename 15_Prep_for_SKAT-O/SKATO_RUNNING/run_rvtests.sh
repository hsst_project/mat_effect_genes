#!/bin/bash -e

#PBS -W umask=002
#PBS -W group_list=imprinting
#PBS -l walltime=02:00:00
#PBS -l nodes=1:ppn=16
#PBS -l mem=16GB

>&2 echo "JOB STARTED"

threads=2

# If running in qsub, we need to change to the current directory
if [ -d "$PBS_O_WORKDIR" ]; then
    >&2 echo INFO: Running as qsub job...
    cd "$PBS_O_WORKDIR"
    threads=16
fi

# Set input and output files
infile=FINAL.variants_for_SKATO.vcf.gz
output=FINAL_SKATO_WITH_COVARIATES

mkdir "$output"

rvtests/executable/rvtest \
  --inVcf "$infile" \
  --pheno phenotypes.ped \
  --geneFile refFlat_hg19.txt \
  --burden exactCMC \
  --kernel skato[alpha=0.005:beta1=1:beta2=20] \
  --covar covariates.ped \
  --covar-name matage,patage,consanguinity,assisted_reproduction,multiple_births,pc1,pc2,pc3,pc4 \
  --numThread "$threads" \
  --out "$output"/"$output"

