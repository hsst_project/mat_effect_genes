Var-pheno mapping
=================

Now we have identified some variants in plausible maternal effect genes, and we know in which sample these occur.
We also have information about the phenotype of these samples, and are particularly interested inthe presence of multiple miscarraiges.

We can cross-reference the phenotype information with the variant information to see if these samples have different variants than non-miscarriage samples.
