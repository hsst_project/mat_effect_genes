#!/bin/bash -e

#--------------------------------------------------------
# prep_vcf_for_merging
# ====================
#
# Author: ben.sanders@nhs.uk
#
# Prepares VCFfor merging
# Intersects to region of interest (BED file from config)
# Select first sample only (for proband files)
# Fixes missing INFO fields in the VCF header
#     NOTE: These are effectively placeholders, but they
#           help stop downstream processes complaining
#
#--------------------------------------------------------

# load config file for user-defined output directories etc.
. ../../vcfmerging.config

# VCF file should be the only argument
# Check for correct number of inputs
if [ "$#" != 2 ]; then
    >&2 echo "ERROR: Script takes exactly one argument"
    >&2 echo "USAGE: prep_vcf_for_merging <vcf file> <proband/trio ID>"
    exit 1
fi

# We want the file name, as well as the full path
# so extract this with basename
vcfpath=$1
vcf=$( basename "$vcfpath" )
proband=$2

# Check file can be opened
if [ ! -f "$vcfpath" ]; then
    >&2 echo "ERROR: File "$vcf" could not be opened"
    exit 1
fi

# NOTE: Don't split + normalise multiallelic variants until after merging, it
#        will work better that way

# Intersect to region of interest and
# remove additional samples (for proband VCF)
# This should work regardless of if there are other samples or not, so
# we can do it for everything
>&2 echo "INFO: Intersecting "$vcf" with BED files "$bedfile""
>&2 echo "INFO: VCF file located at "$vcfpath""
#bedtools intersect -header -wa -a "$vcfpath" -b "$bedfile" | \
zcat "$vcfpath" | \
awk -F '\t' '{if($0 ~ /\#/) print; else if($7 == "PASS") print}' | \
cut -f1-10 > "$workingdir"/"$proband"/"$vcf".temp

# Correct missing INFO values in the header
# GATK can't output to stdout, it has to go to a file
# I would rather have just stdout and redirected in the trio script,
# but it is what it is
>&2 echo "INFO: Correcting VCF header for "$vcf""
/scratch/WRGL/software/gatk-4.1.3.0/gatk FixVcfHeader --INPUT "$workingdir"/"$proband"/"$vcf".temp --OUTPUT "$workingdir"/"$proband"/"$vcf" --VERBOSITY WARNING

# Clean up
rm "$workingdir"/"$proband"/"$vcf".temp
