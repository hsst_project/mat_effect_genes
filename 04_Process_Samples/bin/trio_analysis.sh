#!/bin/bash -e

if [ -d "$PBS_O_WORKDIR" ]; then
    >&2 echo "PBS WORKDIR exists: "$PBS_O_WORKDIR""
    cd $PBS_O_WORKDIR
fi

module load bedtools/2.21.0
module load GATK/4.0
module load ensembl-vep/95
module load python/3.5.1

# This is necessary to run VEP on the compute nodes
# as the seem to have slightly different perl settings than
# the login node (which worked fine without it)
export PERL5LIB=/local/software/perl-modules/lib64/perl5:$PERL5LIB

#------------------------------------------------------
# trio_analysis
# =============
#
# Author: ben.sanders@nhs.uk
#
# Gets the full path to the target VCFs
# Prepares them for merging
# Merges, and splits + normalises multiallelic variants
#
#------------------------------------------------------


# DEV: for identifying failed runs
touch incomplete

# load config file for user-defined output directories etc.
. ../../vcfmerging.config

# Check number of inputs is correct
# Check for correct number of inputs
if [ "$#" != 3 ]; then
    >&2 echo "ERROR: Script takes exactly three argument"
    >&2 echo "USAGE: trio_analysis.sh <proband stable ID> <mother stable ID> <father stable ID>"
    >&2 echo "       'stable ID' is the DDD ID used in the sample VCF filename"
    exit 1
fi

# Append expected file extension to input files
proband="$1"
pro="$proband".vcf.gz
mother="$2"
mat="$mother".vcf.gz
father="$3"
pat="$father".vcf.gz

# Make sample results folder
mkdir -p "$workingdir"/"$proband"

# Check samples are present
# Since we're interested in maternal-effect genes, it's no use if the mothers data
# isn't present. Exit if not found
if [ ! -f "$vcfdir"/"$pro" ]; then
    >&2 echo "ERROR: Proband VCF file "$vcfdir"/"$pro" could not be opened"
    exit 1
fi
if [ ! -f "$vcfdir"/"$mat" ]; then
    >&2 echo "ERROR: Maternal VCF file "$vcfdir"/"$mat" could not be opened"
    exit 1
fi
# If the fathers file is missing, we need to skip it below
if [ ! -f "$vcfdir"/"$pat" ]; then
    >&2 echo "WARNING: Paternal VCF file "$vcfdir"/"$pat" could not be opened"
fi

# Prepare each sample for merging
# This takes the full path to the VCF directory from the config file
# Output files are in workingdir
# Not parallelising as time loss is minimal vs. ease of identifying samples that error.
../../bin/prep_vcf_for_merging.sh "$vcfdir"/"$pro" "$proband"
../../bin/prep_vcf_for_merging.sh "$vcfdir"/"$mat" "$proband"

if [ -f "$vcfdir"/"$pat" ]; then
    >&2 echo "INFO: Paternal VCF found. Processing as trio"
    ../../bin/prep_vcf_for_merging.sh "$vcfdir"/"$pat" "$proband"
    # Merge the trio together
    # Split all multiallelic variants and normalise variant positions
    # This should help to reduce ambiguous calling of the same variants
    >&2 echo "INFO: Merging and normalising trio files for "$proband""
    /scratch/WRGL/software/bcftools-1.9/bin/bcftools merge -m none -O v "$workingdir"/"$proband"/"$pro" "$workingdir"/"$proband"/"$mat" "$workingdir"/"$proband"/"$pat" | \
    /scratch/WRGL/software/bcftools-1.9/bin/bcftools norm -f "$refgenome" -m -any -O v > "$workingdir"/"$proband"/"$proband"_trio.vcf
else
    # Proceed using just the proband and mother
    >&2 echo "WARNING: No paternal VCF found. Processing as pair"
    >&2 echo "INFO: Merging and normalising trio files for "$proband""
    /scratch/WRGL/software/bcftools-1.9/bin/bcftools merge -m none -O v "$workingdir"/"$proband"/"$pro" "$workingdir"/"$proband"/"$mat" | \
    /scratch/WRGL/software/bcftools-1.9/bin/bcftools norm -f "$refgenome" -m -any -O v > "$workingdir"/"$proband"/"$proband"_trio.vcf
fi

# Annotated with VEP
# Don't want any intergenic variants or those in non-coding transcripts
# Use grep to ignore anything that doesn't have a CSQ field (i.e. not annotated)
>&2 echo "INFO: Annotating "$proband" trio with VEP"
vep \
--input_file "$workingdir"/"$proband"/"$proband"_trio.vcf \
--output_file stdout \
--vcf \
--format vcf \
--dir_cache /scratch/imprinting/CAP_245/software \
--fasta "$refgenome" \
--cache \
--offline \
--species homo_sapiens \
--port 3337 \
--hgvs \
--sift b \
--polyphen b \
--no_stats \
--domains \
--uniprot \
--canonical \
--pubmed \
--gencode_basic \
--af_gnomad \
--pick \
--no_intergenic \
--coding_only \
--fields "Consequence,IMPACT,SYMBOL,HGVSc,HGVSp,CANONICAL,SWISSPROT,SIFT,PolyPhen,DOMAINS,gnomAD_AF,PUBMED" \
--fork 8 | \
grep -e "^#" -e "CSQ=" > "$workingdir"/"$proband"/"$proband"_VEP.vcf

#--plugin Condel,/scratch/imprinting/CAP_245/software/.vep/Plugins/config/Condel/config \

# 1) Filter VEP output to protein coding variants only
#    Unfortunately can't use the --ontology setting offline, so we have to specify every consequence...
# 2) Use Condel for missense deleteriousness, but also need to keep non-missense variants as these don't
#    get a Condel score.
# 3) Remove all variants >5% on GnomAD.
# 4) Remove all variants >5% on ExAC (if GnomAD data not available)
# NOTE: This is is three separate calls to filter_vep as it doesn't seem to process them in order, but instead
#       outputs variants passing either filter.
# DEV: Don't pipe output of VEP directly into the filter, as I want to save the full VCF too.
#>&2 echo "INFO: Filtering "$proband" VEP results. Removing non-coding, Condel benign, and gnomAD >5% variants"
#filter_vep \
#--input_file "$workingdir"/"$proband"/"$proband"_VEP.vcf \
#--output_file stdout \
#--format vcf \
#--filter "Consequence is protein_altering_variant or Consequence is missense_variant or Consequence is inframe_deletion or \
#Consequence is inframe_insertion or Consequence is transcript_amplification or Consequence is start_lost or \
#Consequence is stop_lost or Consequence is frameshift_variant or Consequence is stop_gained or Consequence is splice_donor_variant or \
#Consequence is splice_acceptor_variant or Consequence is transcript_ablation" | \
#filter_vep \
#--format vcf \
#--output_file stdout \
#--filter "Condel is deleterious or Consequence is not missense_variant" \
#--filter "gnomAD_AF <= 0.05 or not gnomAD_AF" | \
#filter_vep \
#--format vcf \
#--force_overwrite \
#--output_file "$workingdir"/"$proband"/"$proband"_VEP_filtered.vcf \
#--filter "'ExAC_AF' <= 0.05 or not 'ExAC_AF'"

# Extract only the variants present in the mother (regardless of other samples)
>&2 echo "INFO: Filtering "$proband" to maternal genes only"
#../../bin/filter_vars.py "$workingdir"/"$proband"/"$proband"_VEP_filtered.vcf --include mat > "$workingdir"/"$proband"/"$proband"_mat_vars.vcf

# summarise the genes
>&2 echo "INFO: Generating maternal variant summary for "$proband""
python ../../bin/VEP_VCF_to_TSV.py "$workingdir"/"$proband"/"$proband"_VEP.vcf > "$workingdir"/"$proband"/"$proband"_mat_summary.tsv

# tidy up temp files
rm "$workingdir"/"$proband"/"$pro"*
rm "$workingdir"/"$proband"/"$mat"*
if [ -f "$vcfdir"/"$pat" ]; then
    rm "$workingdir"/"$proband"/"$pat"*
fi

>&2 echo "INFO: Processing of "$proband" complete. Annotated file at "$workingdir"/"$proband"/"$proband"_VEP_filtered.vcf"

# DEV: quick indication that analysis is done
rm incomplete
touch complete
