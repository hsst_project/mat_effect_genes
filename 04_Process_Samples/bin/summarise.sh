#!/bin/bash -e

#--------------------------------------------------------------
# summarise
# ============
#
# Author: ben.sanders@nhs.uk
#
# #TODO
#
#--------------------------------------------------------------

# load config file for user-defined output directories etc.
. vcfmerging.config

# Merge maternal gene summaries
# The header in the per-trio summaries has a # to indicate the header line
# and so make it easy to grep out and combine here. But for the combined file we
# don't want a # as it will be read into Pandas and I don't want to have to edit out
# an extra #
>&2 echo "INFO: Merging maternal gene summaries"

# Get the header row from one file
grep "^CHROM" "$( find "$workingdir" -name "*.tsv" | head -1 )"

# Print all the data without headers
# It seems like some scripts don't output equals signs properly
# so let's convert it back for p.= variants
for p in  "$workingdir"/*/*.tsv; do
    cat "$p" | sed s/"%3D"/"="/g
done | grep -v "^CHROM"
