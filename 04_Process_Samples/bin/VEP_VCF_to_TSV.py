#!/usr/bin/env python3

"""
Filter DDD trio VCF variants
============================

ben.sanders@nhs.net
-------------------

Evaluate a DDD trio VCF to establish possible inheritence of variants.

DDD proband VCF contains 3 samples - proband, father, and mother in a multi-sample VCF

Applies user defined filters:
    --include - print if specified sample includes the variant (mat, pat, pro)
    --exclude - print only if variant is not in specified sample (mat, pat, pro)
Filters can be combined, and multiple options can be specified for each
If you include and exclude the same sample, you obviously won't get any output. Don't be silly.

Currently only works with a stream as input, so you have to run cat first to open a file.
    #TODO - might add this, but probably needs a dedicated argument?
"""

import sys
import argparse
import pprint

class VcfInheritence(object):
    """
    Work out the possible inheritence of a variant in a DDD VCF
    """
    def __init__(self, vcf):
        # Track the number of variants processed
        self.variants_read = 0
        self.variants_error = 0

        # open the input file (if specified)
        try:
            if vcf:
              self.vcf = open(vcf, "r")
            else:
                self.vcf = sys.stdin
                if sys.stdin.isatty():
                    print("ERROR: No data in stdin stream", file=sys.stderr)
                    sys.exit(1)
        except FileNotFoundError:
            print("ERROR: Specified VCF file {} could not be opened".format(vcf), file=sys.stderr)
            sys.exit(1)

        # Display some info about variant numbers to the stderr stream
        self.read_vcf()
        print("INFO: Read {} variant lines".format(self.variants_read), file=sys.stderr)
        print("INFO: {} variants did not process correctly".format(self.variants_error), file=sys.stderr)

    def read_vcf(self):
        """
        Currently only handles an stdin stream.
        ? Worth modifying to handle files, too?
        """

        # This try/except BrokenPipeError means we can use head on the outut
        # without a nasty error message (mostly)

        try:
            for line in self.vcf:
                line = line.rstrip()
                if line.startswith("##"):
                    if "CSQ" in line:
                        csqheader = self.parse_csq(line)
                    else:
                        pass
                elif line.startswith("#"):
                    dataheader = line.rstrip().lstrip("#").split()
                    self.print_header()
                else:
                    pass
                    self.process_line(dataheader, csqheader, line)
        except BrokenPipeError:
            sys.exit(1)

    def parse_csq(self, line:str) -> list:
        """
        Read the INFO header line that defines the VEP CSQ= data items
        """
        # looking at the output format, the actual consequence definition starts after
        # a ":", so we'll; split on that.
        line = line.split(":")
        # This may introduce some whitespace, so use strip() to remove it.
        # There's some trailing stuff we want to remove from the VCF header formatting
        # so remove "> from the end, then split on the pipe to get a list of the CSQ fields
        csq = [x.upper() for x in line[1].strip().rstrip(">").rstrip('\"').split("|")]
        return csq

    def process_line(self, dataheader: list, csqheader:list, line: str):
        """
        Split each line and extract the relevant parts
        Should be formatted according to standard VCF specifications
        e.g. CHR,POS,REF,ALT,etc.
        """

        dataline = line.rstrip().split()
        # The FORMAT column defines the structure of the following sample columns
        format = dataline[8].split(":")
        # Zip each sample into a dict using the format column as the key
        # FORMAT may vary between varaints, but is always consistent across samples
        # at the same variant. Add the sample ID into the dict too.
        # sample as a dict is probably more straightforward than a new class
        var = {}
        var['CHROM'] = dataline[0]
        var['POS'] = dataline[1]
        var['ID'] = dataline[2]
        var['REF'] = dataline[3]
        var['ALT'] = dataline[4]
        var['QUAL'] = dataline[5]
        var['FILTER'] = dataline[6]
        var['INFO'] = {}
        infofield = dataline[7].split(";")
        for item in infofield:
            item = item.split("=")
            item[0] = item[0].upper()
            try:
                var['INFO'][item[0]] = item[1]
            except IndexError:
                var['INFO'][item[0]] = True

        var['INFO']['CSQ'] = dict(zip(csqheader, var['INFO']['CSQ'].split("|")))
        var['FORMAT'] = dataline[8]
        try:
            var['PRO'] = dict(zip(format, dataline[9].split(":")))
            var['PRO']["ID"] = dataheader[9]
        except IndexError:
            var['PRO'] = {'ID': None, 'GT': None}
        try:
            var['MAT'] = dict(zip(format, dataline[10].split(":")))
            var['MAT']["ID"] = dataheader[10]
        except IndexError:
            var['MAT'] = {'ID': None, 'GT': None}
        try:
            var['PAT'] = dict(zip(format, dataline[11].split(":")))
            var['PAT']["ID"] = dataheader[11]
        except IndexError:
            var['PAT'] = {'ID': None, 'GT': None}

        self.print_line(var)

    def print_line(self, var: dict):
        print("{}\t{}\t{}:{}{}>{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format(var.get('CHROM', None),
                                                 var.get('POS', None),
                                                 var.get('CHROM', None),
                                                 var.get('POS', None),
                                                 var.get('REF', None),
                                                 var.get('ALT', None),
                                                 var['INFO']['CSQ'].get('SYMBOL', None),
                                                 var['INFO']['CSQ'].get('SWISSPROT', None),
                                                 var['INFO']['CSQ'].get('IMPACT', None),
                                                 var['INFO']['CSQ'].get('HGVSC', None),
                                                 var['INFO']['CSQ'].get('HGVSP', None),
                                                 var['INFO']['CSQ'].get('SIFT', None),
                                                 var['INFO']['CSQ'].get('POLYPHEN', None),
                                                 var['INFO']['CSQ'].get('CONDEL', None),
                                                 var['INFO']['CSQ'].get('GNOMAD_AF', None),
                                                 var['INFO'].get('EXAC_AF', None),
                                                 var['INFO']['CSQ'].get('PUBMED', None),
                                                 var['MAT'].get('ID', None),
                                                 var['MAT'].get('GT', None),
                                                 var['PAT'].get('ID', None),
                                                 var['PAT'].get('GT', None),
                                                 var['PRO'].get('ID', None),
                                                 var['PRO'].get('GT', None)))

    def print_header(self):
        """

        Print a header row for the output.
        Added as a function so it's next to the print_line function where the printed
        fields are defined. Also in case I make the output fields user selectable in the future.
        """
        print("CHROM\tPOS\tID\tGENE\tPROTEIN\tIMPACT\tHGVSc\tHGVSp\tSIFT\tPolyphen\tCondel\tGnomAD_AF\tExAC_AF\tPUBMED\tMAT_ID\tMAT_GT\tPAT_ID\tPAT_GT\tPRO_ID\tPRO_GT")

if __name__ == "__main__":
    parser =  argparse.ArgumentParser(description='Filter varaints in a DDD trio VCF by presence/absence in samples')
    parser.add_argument('vcf', type=str, nargs='?', help='VCF file to process')
    args = parser.parse_args()

    vcf = VcfInheritence(args.vcf)
