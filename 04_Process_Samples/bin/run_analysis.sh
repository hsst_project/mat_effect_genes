#!/bin/bash -e

#--------------------------------------------------------------
# run_analysis
# ============
#
# Author: ben.sanders@nhs.uk
#
# Process a trio of samples:
#  1) filters to ROI
#  2) merges proband and parents into single file
#  3) annotates with InterVar for automated ACGS classification
#
#--------------------------------------------------------------


# Check for correct number of inputs
if [ "$#" != 1 ]; then
    >&2 echo "ERROR: Script takes exactly one argument"
    >&2 echo "USAGE: run_analysis <trio list>"
    exit 1
fi

# load config file for user-defined output directories etc.
. vcfmerging.config

# Get the input list of samples from the user
triolist=$(pwd)/$1

# Check the file exists
if [ ! -f "$triolist" ]; then
    >&2 echo "ERROR: File $triolist does not exist or cannot be opened"
    >&2 echo "USAGE: run_analysis <trio list>"
    exit 1
fi

>&2 echo "INFO: Processing trio file "$triolist""

mkdir -p "$workingdir"
cd "$workingdir"
# Select just those lines with "TRIO". This removes any header lines, and also
# any singleton samples that might have sneaked in.
grep -w "TRIO" "$triolist" > triolist.filtered

# Loop through the input list of files, and separate out each sample
while read p; do
    parray=($p)
    pro=${parray[0]}
    mat=${parray[1]}
    pat=${parray[2]}

    mkdir -p "$pro"
    cd "$pro"
    #DEV: If I follow Ahmeds example, then this should be qsub'd for each sample
    #     This is where all the heavy lifting and *actual* processing is done
    >&2 echo "INFO: processing trio associated with sample "$pro""
    qsub -l walltime=02:30:00,nodes=1:ppn=8,mem=4GB -F "$pro $mat $pat" ../../bin/trio_analysis.sh
    cd ..
done < triolist.filtered
