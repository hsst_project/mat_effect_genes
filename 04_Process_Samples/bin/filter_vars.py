#!/usr/bin/env python3

"""
Filter DDD trio VCF variants
============================

ben.sanders@nhs.net
-------------------

Evaluate a DDD trio VCF to establish possible inheritence of variants.

DDD proband VCF contains 3 samples - proband, father, and mother in a multi-sample VCF

Applies user defined filters:
    --include - print if specified sample includes the variant (mat, pat, pro)
    --exclude - print only if variant is not in specified sample (mat, pat, pro)
Filters can be combined, and multiple options can be specified for each
If you include and exclude the same sample, you obviously won't get any output. Don't be silly.

Currently only works with a stream as input, so you have to run cat first to open a file.
    #TODO - might add this, but probably needs a dedicated argument?
"""

import sys
import argparse

class VcfInheritence(object):
    """
    Work out the possible inheritence of a variant in a DDD VCF
    """
    def __init__(self, include, exclude, vcf):
        # Track the number of variants processed
        self.variants_read = 0
        self.variants_error = 0
        self.variants_filtered = 0

        # open the input file (if specified)
        try:
            if vcf:
              self.vcf = open(vcf, "r")
            else:
                self.vcf = sys.stdin
                if sys.stdin.isatty():
                    print("ERROR: No data in stdin stream", file=sys.stderr)
                    sys.exit(1)
        except FileNotFoundError:
            print("ERROR: Specified VCF file could not be opened", file=sys.stderr)
            sys.exit(1)

        # Create a list of filters to apply to each variant
        self.filters = {}
        try:
            self.filters = [VcfFilter('+', x) for x in include]
        except TypeError:
            # TypeError occurs if filter list is empty
            self.filters = []
        try:
            self.filters.extend([VcfFilter('-', x) for x in exclude])
        except TypeError:
            pass

        # Display some info about variant numbers to the stderr stream
        print("INFO: Include sample filters: {}".format(include), file=sys.stderr)
        print("INFO: Exclude sample filters: {}".format(exclude), file=sys.stderr)
        self.read_vcf()
        print("INFO: Read {} variant lines".format(self.variants_read), file=sys.stderr)
        print("INFO: {} variants removed by filters".format(self.variants_filtered), file=sys.stderr)
        print("INFO: {} variants did not process correctly".format(self.variants_error), file=sys.stderr)

    def read_vcf(self):
        """
        Currently only handles an stdin stream.
        ? Worth modifying to handle files, too?
        """

        # This try/except BrokenPipeError means we can use head on the outut
        # without a nasty error message (mostly)

        try:
            for line in self.vcf:
                line = line.rstrip()
                if line.startswith("##"):
                    print(line)
                elif line.startswith("#"):
                    print(line)
                    dataheader = line.rstrip().lstrip("#").split()
                else:
                    self.process_line(dataheader, line)
        except BrokenPipeError:
            sys.exit(1)

    def process_line(self, dataheader: list, line: str):
        """
        Split each line and extract the relevant parts
        Should be formatted according to standard VCF specifications
        e.g. CHR,POS,REF,ALT,etc.
        """

        dataline = line.rstrip().split()
        # The FORMAT column defines the structure of the following sample columns
        format = dataline[8].split(":")
        # Zip each sample into a dict using the format column as the key
        # FORMAT may vary between varaints, but is always consistent across samples
        # at the same variant. Add the sample ID into the dict too.
        # sample as a dict is probably more straightforward than a new class
        var = {}
        try:
            var['pro'] = dict(zip(format, dataline[9].split(":")))
            var['pro']["ID"] = dataheader[9]
        except IndexError:
            var['pro'] = None
        try:
            var['mat'] = dict(zip(format, dataline[10].split(":")))
            var['mat']["ID"] = dataheader[10]
        except IndexError:
            var['mat'] = None
        try:
            var['pat'] = dict(zip(format, dataline[11].split(":")))
            var['pat']["ID"] = dataheader[11]
        except IndexError:
            var['pat'] = None

        # try/except KeyError handles the rare cases where the variant has
        # an unsupported genotype value
        try:
            # Only print lines in which all filters return True
            if not False in [filter.filter_variant(var) for filter in self.filters]:
                # line is the original string of the VCF variant
                print(line)
            # Update the counts of the various permutationss
            else:
                self.variants_filtered += 1
            self.variants_read += 1
        except KeyError:
            self.variants_error += 1
            print("WARNING: Invalid genotype value encountered at position {}:{}".format(dataline[0], dataline[1]), file=sys.stderr)

class VcfFilter(object):
    """
    Filter a line based on presence or absence of variant in the specified sample.
    """
    def __init__(self, type: str, sample: str):
        self.type = type
        self.sample = sample

    def filter_variant(self, var: dict) -> bool:
        """
        Return True if the variant passes the filter.
        """
        if self.type == '+':
            return self.varisin(var[self.sample])
        elif self.type == '-':
            return not self.varisin(var[self.sample])
        return False

    @staticmethod
    def varisin(sample: dict) -> bool:
        """
        Check if the variant is in a sample when passed as sample dict,
        by looking at that samples GT field.

        VCF should be split + normalised for multiallelics otherwise there
        could be some issues at this point
        """
#        valid_gts = ["1/1", "0/1", "1/0", "1/.", "./1", "./.", "0/0"]
#        assert sample['GT'] in valid_gts, "ERROR: Invalid genotype seen: {}".format(sample['GT'])

        # DEV: Try excluding genotypes indicating absence, rather than including presence
        #      This should allow better handling of multiallelic vars without split/normalising them
#        absent = ["0/0", "./."]
#        return sample['GT'] not in absent

        # For a non-multiallelic variant, these should be the only
        # genotypes that indicate (with confidence) the variant is present
        present = ["1/1", "0/1", "1/0", "1/.", "./1"]
        # Returns True if sample GT is in the above list
        return sample['GT'] in present

if __name__ == "__main__":
    parser =  argparse.ArgumentParser(description='Filter varaints in a DDD trio VCF by presence/absence in samples')
    parser.add_argument('vcf', type=str, nargs='?', help='VCF file to process')
    parser.add_argument('--include', nargs='*', choices=['mat','pat','pro'], type=lambda s : s.lower(), help='Samples to include')
    parser.add_argument('--exclude', nargs='*', choices=['mat','pat','pro'], type=lambda s : s.lower(), help='Samples to exclude')
    args = parser.parse_args()

    vcf = VcfInheritence(args.include, args.exclude, args.vcf)
