Sample processing
=================

This folder contains the scripts to actually run the analysis, drawing on the previous work detailed in the other folders.


DDD trio VCF only includes sites in the proband?
    I think what I previously thought were uninherited vars were really just split multiallelic vars, and there was another allele present
    Need to analyse maternal VCF separately to get mat only variants

DDD Proband VCFs have genotype data for all three trio members.
However, this is only present for variants present in the proband.
Uninherited variants (e.g. only present in mother, not proband) are not included.
This means we can't see purely maternal or paternal variants in this file.

We need to merge the three VCFs together properly, so we can filter on a single file.

Can merge VCFs using bcftools merge.

This encountered errors: missing INFO values that aren't defined in the header.

Use GATK FixVcfHeader to correct this.

Missing variants are uncertain, as we can't confirm whether they were a) genuinely not present or b) not covered enough to be sure either way.
These will count as missing, but the ./. genotype indicates the uncertainty.

scripts
-------

### run_analysis.sh

Starts the analysis. Runs on each trio listed in the trio list, which must be passed as the only argument
Trio list is in the format <proband stable ID> <mother stable ID> <father stable ID> <[TRIO/SINGLETON]>
where the stable ID is the ID used in the DDD vcf filename.

usage:

$ ./run_analysis.sh <trio list>

### trio_analysis.sh
Reconstructs the path to the DDD VCF files, using the directories defined in vcfmerging.config
Calls prep_vcf_for_merging.sh on each file, and stores the processed file in the working firectroy from vcfmerging.config
Merges the prepare VCFs into a single VCF file, ready for inheritence based filtering.
splits mulitallellic + normalises variants

### prep_vcf_for_merging.sh
Intersects VCF to the defined ROI, and extracts only the first sample (applicable to proband VCF)

ERRORS
------

Trio DDDP138909 appears to be missing a paternal sample. Scripts were updated to account for this and it was analysed as just proband/mother.
