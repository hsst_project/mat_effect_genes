Conda setup
===========

This project uses a Conda virtual environment to manage packages and dependencies that might otherwise be difficult to install on the shared user environment of Iridis.
This also helps to make it reproducible and portable, as someone else can configure exactly the same environment on thier own system.

Conda module
------------

On Iridis, conda needs to be activated by loading the conda module. We require at least v.4, due to requirements of some packages.

`$ module load conda/4.4.0`

you may need to run `conda init` to configure your terminal. Try to load the env as below, and follow instructions if prompted.

Install conda env
-----------------

To recreate the analysis environment, you must create it using the template yml file:

`$ conda env create -f mateffect.yml`

This will install all programs and dependencies into a new conda environment, separated from the shared default environment. 

Load conda env
--------------

The name of the env for this project is "mateffect". To load it, simply run:

`$ conda activate mateffect`

Update python packages
----------------------

Python packages are managed using the pip installer, which will install to the instance of python within the conda env.
Again, this is totally separate from the system python installation, and you won't need to load any modules.
To restore python packages run:

`$ pip3 install -r requirements.txt`
