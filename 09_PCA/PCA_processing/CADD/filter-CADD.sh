infile="$1"

cutoff="$2"

# Write a minimal VCF header
echo "##fileformat=VCFv4.2"
echo -e "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER"

# Use awk to re-format the TSV into VCF format
grep -v "^#" "$infile" | awk -v cutoff="$cutoff" 'BEGIN{FS="\t"; OFS="\t"}{if ($6 < cutoff) print $1,$2,".",$3,$4,$6,"PASS"}'
