# Filtering by CADD

We are using CADD scores to further filter the variants used for PCA.

https://cadd.gs.washington.edu/score was used to aadd CADD scores to the variants in FINAL.AUTOSOMAL-ONLY.vcf.gz, where any sex-chromsome varaints had been removed.

The variants were then filtered with awk to remove anywith scores > ??

 * CADD score is normalised to a 1-99 score, where each variant is ranked relative to all other possible SNVs.
 * CADD 10 = variant score in top 1%, CADD 20 = top 0.1%, CADD 30 = top 0.01%, etc.

This had to be formatted into VCF format (from CADD output TSV) so that vcfintersect could be used to pull just these variants from the oiriginal FINAL.AUTOSOMAL-ONLY.vcf.gz file.
