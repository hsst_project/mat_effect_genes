Relatedness scores for DDD and 1kGP samples, using linkage pruned variants with GnomAD 0.1 < 0.2 and autosomal vars only.
