#!/bin/bash -e

#PBS -W umask=002
#PBS -W group_list=imprinting
#PBS -l walltime=06:00:00
#PBS -l nodes=1:ppn=16
#PBS -l mem=16GB

## Control settings

# DEV: TODO: This file should be updated to point to the main VCF folder
input_file=vcfs_to_merge.list
onekgpfile=1kGP-VCFs/PHASE1.1KGP-gnomad-vars.vcf.gz
threads=4

>&2 echo "JOB STARTED"

# If running in qsub, we need to change to the current directory
if [ -d "$PBS_O_WORKDIR" ]; then
    >&2 echo INFO: Running as qsub job...
    cd "$PBS_O_WORKDIR"
    threads=16
fi

# I've encountered some ImportError problems, caused by python2.7 being
# in the PYTHONPATH
unset PYTHONPATH
module load conda/4.4.0
# It's necessary to point directly to conda and source it
# to get around the conda init problem
source /local/software/conda/anaconda3/etc/profile.d/conda.sh
conda activate /scratch/WRGL/.conda/envs/vcflib/

module load bcftools/1.10.2
module load htslib/1.9
module load plink/1.90beta

# Split input file into <1000 sample chunks
>&2 echo "INFO: Merging VCF files..."

# The full list hits the compute node ulimit of 1024.
# If we split the list into sub-lists of <1024 samples then
# they will each process, we can then merge the resulting files.
# Generalise to allow it to run on more samples if needed.
# DEV: set to 5 for testing only!
split -l 500 --numeric-suffixes "$input_file" TEMP.sample_list

# Merge VCF files from all  DDD samples
# Keeps only PASS filter variants, and sets any unknown genotypes to 0/0 hom reference
# --merge option left unset as we will remove any multiallelic vars next.
merge() {
    input="$1"
    >&2 echo "INFO: Merging files from $input..."
    bcftools merge \
    --threads "$threads" \
    --apply-filters PASS \
    --output-type z \
    --missing-to-ref \
    --output "$input".vcf.gz \
    --file-list "$input"

# DEV: Removed so we can get rid of any variants with an uncertain call


    tabix -f -p vcf "$input".vcf.gz
}

for p in $( find . -name "*sample_list*" | sort ); do
    merge "$p"
    echo "$p".vcf.gz >> TEMP.merge_merged_files.list
done

# Now merge the merged files to get the full merged VCF
merge TEMP.merge_merged_files.list
# rename that output file
mv TEMP.merge_merged_files.list.vcf.gz TEMP.merged.vcf.gz
mv TEMP.merge_merged_files.list.vcf.gz.tbi TEMP.merged.vcf.gz.tbi

# Split multiallelic variants
>&2 echo "INFO: Splitting multiallelic variants.."
bcftools norm \
--threads "$threads" \
--check-ref s \
--fasta-ref /scratch/WRGL/REFERENCE_FILES/REFERENCE_GENOME/GRCh37_no_gl000201.fa \
--multiallelics -any \
--output-type z \
--output TEMP.split.vcf.gz \
TEMP.merged.vcf.gz

# Remove variants with '*' alleles
>&2 echo "INFO: Removing variants with '*' alleles..."
zcat TEMP.split.vcf.gz | \
grep -v -P "\t\*\t" | \
bgzip > TEMP.nostars.vcf.gz
tabix -p vcf TEMP.nostars.vcf.gz

# Filter to just variants in GnomAD target list
#>&2 echo "INFO: Extracting variants present in gnomAD target list..."
#vcfintersect \
#--reference /scratch/WRGL/REFERENCE_FILES/REFERENCE_GENOME/GRCh37_no_gl000201.fa \
#--intersect-vcf GnomAD-variants/gnomad.SNP.vcf.gz \
#TEMP.nostars.vcf.gz | bgzip > FINAL.DDD-sample-variants-in-gnomad.vcf.gz
#tabix -p vcf FINAL.DDD-sample-variants-in-gnomad.vcf.gz

#######################
##
## Merge with 1kGP data
##
#######################

# Filter 1kGP VCFs to just include the same variants above (using the whole GnomAD list gives you
# intronic variants that the DDD data doesn't cover.

# Check that the file exists
#if [ ! -f "$onekgpfile" ]; then
#    >&2 echo ERROR: "$onekgpfile" not found
#    exit 1
#fi

#>&2 echo "INFO: Merging with 1kGP VCF. Using variants in DDD samples only..."
#vcfintersect \
#--reference /scratch/WRGL/REFERENCE_FILES/REFERENCE_GENOME/GRCh37_no_gl000201.fa \
#--intersect-vcf FINAL.DDD-sample-variants-in-gnomad.vcf.gz \
#"$onekgpfile" | bgzip > FINAL.1kGP-variants-in-gnomad.vcf.gz
#tabix -p vcf FINAL.1kGP-variants-in-gnomad.vcf.gz

#echo FINAL.DDD-sample-variants-in-gnomad.vcf.gz > TEMP.merge-DDD-and-1KGP-VCFs.list
#echo FINAL.1kGP-variants-in-gnomad.vcf.gz >> TEMP.merge-DDD-and-1KGP-VCFs.list

#merge TEMP.merge-DDD-and-1KGP-VCFs.list
#mv TEMP.merge-DDD-and-1KGP-VCFs.list.vcf.gz FINAL.MERGED.DDD-1kGP-variants-in-gnomad.vcf.gz
#mv TEMP.merge-DDD-and-1KGP-VCFs.list.vcf.gz.tbi FINAL.MERGED.DDD-1kGP-variants-in-gnomad.vcf.gz.tbi

#rm TEMP.*
