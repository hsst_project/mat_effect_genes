# Preparing for PCA

## steps

1. Copied VCFs from just maternal samples (by DDD ID from previously loaded data)
2. Corrected VCF headers using GATK
3. Too many samples to merge at once - bcftools can't handle that many files. Split the list into two.
4. bgzip and tabix index (or adapt bcftools command to output compressed)
5. Merge VCFs from both halves of the split list, then merge those two files.

## TODO

* Get list of hapmak/1kg SNPs for PCA
* Extract them from merged VCF
* ?? Do I then need to prune for linkage as well ??
* Do PCA
* Project against 1kg known ethnicities
