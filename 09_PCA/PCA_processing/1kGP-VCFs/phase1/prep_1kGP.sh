#!/bin/bash -e

#PBS -W umask=002
#PBS -W group_list=wrgl
#PBS -l walltime=06:00:00
#PBS -l nodes=1:ppn=16
#PBS -l mem=32GB

# I've encountered some ImportError problems, caused by python2.7 being
# in the PYTHONPATH
unset PYTHONPATH
module load conda/4.4.0
# It's necessary to point directly to conda and source it
# to get around the conda init problem
source /local/software/conda/anaconda3/etc/profile.d/conda.sh
conda activate vcflib

module load bcftools/1.10.2
module load htslib/1.9
module load plink/1.90beta

threads=4
# If running in qsub, we need to change to the current directory
if [ -d "$PBS_O_WORKDIR" ]; then
    cd "$PBS_O_WORKDIR"
    threads=16
fi


##################################
##
## Prep 1kGP VCFs for PCA analysis
##
##################################

# 1kGP data is very large, and can be downloaded as individual chromosomes. Before merging all chrs,
# we want to do some processing. This script goes chr by chr, and removes and variants with "*" alleles,
# as these cause problems with plink.
#
# Then the processed files are intersected against the GnoAD variant list to keep only those variants
# which are in GnomAD. This includes intronic and intergenic variants, but those will be removed when
# the DDD and 1kGP files are merged.
#
# This script should be run before prep_vcfs_for_pca.sh.

processvcf() {
    vcffile="$1"
    >&2 echo "INFO: Processing "$vcffile"..."

    # Remove variants with '*' alleles
    >&2 echo "INFO: Removing variants with '*' alleles..."
    zcat "$vcffile" | \
    grep -v -P "\t\*\t" | \
    grep -v "<DEL>" | \
    bgzip > NOSTAR."$vcffile"

    tabix -p vcf NOSTAR."$vcffile"

    # Filter to just variants in GnomAD target list
    >&2 echo "INFO: Filtering to only GnomAD variants.."
    vcfintersect \
    --reference /scratch/WRGL/REFERENCE_FILES/REFERENCE_GENOME/GRCh37_no_gl000201.fa \
    --intersect-vcf gnomad.SNP.vcf.gz \
    NOSTAR."$vcffile" | bgzip > FINAL."$vcffile"

    tabix -p vcf FINAL."$vcffile"

    rm TEMP."$vcffile"
}

export -f processvcf

/scratch/WRGL/software/parallel/bin/parallel \
--jobs "$threads" \
--ungroup processvcf \
::: $( find . -name "ALL.chr*.vcf.gz" | sort -k1,1V | sed s/"\.\/"/""/g )

# TODO Merge the FINAL.chr files and save as PHASE1.1KGP-gnomad-vars.vcf.gz

