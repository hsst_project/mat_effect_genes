list=phase1_vcfs.txt

while read p; do
    >&2 echo INFO: downloading "$p"
    wget -N -c "$p" &> /dev/null
done < "$list"
