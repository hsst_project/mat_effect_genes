#!/bin/bash -e

module load plink/1.90beta

# This just runs plink, it's quick and easy
# TODO: make the user supply the VCF file.

vcffile="$1"

if [ ! -f "$vcffile" ]; then
    >&2 echo "ERROR: file $vcffile could not be opened"
    exit 1
fi

# Prune linkage with plink
plink \
--vcf "$vcffile" \
--double-id \
--set-missing-var-ids @:# \
--indep-pairphase 50 10 0.1 \
--out PLINK.pruned

# Do PCA with plink
plink \
--vcf "$vcffile" \
--double-id \
--set-missing-var-ids @:# \
--extract PLINK.pruned.prune.in \
--make-bed \
--pca \
--out PLINK.pca
