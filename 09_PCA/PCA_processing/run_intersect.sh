#!/bin/bash -e

#PBS -W umask=002
#PBS -W group_list=imprinting
#PBS -l walltime=02:00:00
#PBS -l nodes=1:ppn=4
#PBS -l mem=20GB

# If running in qsub, we need to change to the current directory
if [ -d "$PBS_O_WORKDIR" ]; then
    >&2 echo INFO: Running as qsub job...
    cd "$PBS_O_WORKDIR"
fi

module load htslib/1.9
# I've encountered some ImportError problems, caused by python2.7 being
# in the PYTHONPATH
unset PYTHONPATH
module load conda/4.4.0
# It's necessary to point directly to conda and source it
# to get around the conda init problem
source /local/software/conda/anaconda3/etc/profile.d/conda.sh
conda activate /scratch/WRGL/.conda/envs/vcflib/

targets=$( zcat TEMP.HIFREQ.DDD.vcf.gz | grep -v "^#" | wc -l )
>&2 echo INFO: "$targets" variants identified in target VCF

vcfintersect \
--bed Twist_Exome_Target_hg19.bed \
--reference /scratch/WRGL/REFERENCE_FILES/REFERENCE_GENOME/GRCh37_no_gl000201.fa \
--intersect-vcf TEMP.HIFREQ.DDD.vcf.gz \
1kGP-VCFs/PHASE1.1KGP-gnomad-vars.vcf.gz > FINAL.HIFREQ.1kGP.vcf

intersected=$( cat FINAL.HIFREQ.1kGP.vcf | grep -v "^#" | wc -l )
>&2 echo INFO "$intersected" variants retained.

#tabix -p vcf FINAL.HIFREQ.1kGP.vcf.gz
