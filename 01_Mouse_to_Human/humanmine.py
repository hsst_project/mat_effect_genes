#!/usr/bin/env python3

"""
ben.sanders@nhs.uk


Query the humanmine.org API to find the human homologue for a mouse protein UniProt ID.

Expects a text file list of protein IDs.

Outputs to console MOUSE_ID   HUMAN_GENE    REFSEQ_IDs

"""

import sys
from intermine.webservice import Service
from intermine.query import Query

def query_humanmine(querygene: str) -> Query:
    """
    Look up data in humanmine.
    Query below is copied from the query builder on the website.
    """
    service = Service("http://www.humanmine.org/humanmine/service")
    query = service.new_query("Protein")
    query.add_view(
        "uniprotName", "genes.homologues.homologue.symbol",
        "genes.homologues.homologue.transcripts.primaryIdentifier",
        "genes.homologues.homologue.interactions.participant2.symbol",
        "genes.homologues.homologue.proteinAtlasExpression.cellType",
        "genes.homologues.homologue.proteinAtlasExpression.level"
    )
    query.add_constraint("uniprotName", "=", querygene, code = "A")
    query.add_constraint("genes.homologues.homologue.organism.commonName", "=", "human", code = "B")
    query.add_constraint("genes.homologues.homologue.transcripts.primaryIdentifier", "CONTAINS", "NM_", code = "C")
    query.outerjoin("genes.homologues.homologue.proteinAtlasExpression")
    # return as is, we'll convert it to a useful format later
    return query

def query_to_dict(query: Query) -> dict:
    """
    Convert the result of a humanmine query into a simple dict
    Combine all the results for a single gene together (they are otherwise separated
    into every individual combination of the data)
    """
    genedict = {}

    # The Query returned is an iterator containing the full API results
    for row in query.rows():
        gene = row["genes.homologues.homologue.symbol"]
        mousegene = row["uniprotName"]
        transcript = row["genes.homologues.homologue.transcripts.primaryIdentifier"]
        interaction = row["genes.homologues.homologue.interactions.participant2.symbol"]
        proteinatlascells = row["genes.homologues.homologue.proteinAtlasExpression.cellType"]
        proteinatlaslevels = row["genes.homologues.homologue.proteinAtlasExpression.level"]
        # If the gene isn't in the dict yet, we need to add it as otherwise
        # getting the desired structure is tricky. I don't know if I can set this up
        # as I want using dict.get with a defult value.
        try:
            genedict[gene]
        except KeyError:
            genedict[gene] = {"mousegene": None, "transcripts": set(), "interactions": set()}
        # Now the gene is set up, we can add the data
        genedict[gene]["mousegene"] = mousegene
        genedict[gene]["transcripts"].add(transcript)
        genedict[gene]["interactions"].add(interaction)

    return genedict

def read_genelist(fname: str) -> list:
    """
    Reads a list of genes from a file
    but doesn't do any kind of sanity check etc.
    """
    genelist = []
    with open(fname, "r") as f:
        for line in f:
            genelist.append(line.rstrip())
    return genelist

if __name__ == "__main__":
    assert len(sys.argv) == 2, "ERROR: This script takes exactly one argument (gene list file)"
    for gene in read_genelist(sys.argv[1]):
        result = query_humanmine(gene)
        for k,v in  query_to_dict(result).items():
            print("{}\t{}\t{}".format(v['mousegene'], k, ",".join(v['transcripts'])))
