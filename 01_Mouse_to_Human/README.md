Mouse proteins to human genes
=============================

maternal_effect_genes.xlsx contains a list of proteins identified in mouse oocytes, indicating that variants in these genes might affect the offspring via a maternal effect.

There is also the amount of expression in oocytes from young mice vs. old mice.

Genes have been highlighted as "essential" or "interesting" based on thier expected role in development. This includes, for example many of the NLRP family of genes.

The other genes are not thought to have any oocyte/embryo specificity.

In the first instance, analysis was performed using only the essential/interesting genes.

Analysis
--------

### Finding homologues

This list of mouse proteins had to be translated into a list of homologous human genes.

humanmine.py uses the humanmine.org API to look up the human homologue of the mouse gene UniProt identifier. It outputs a tab-separated list consisting of:

`MOUSE_ID    HUMAN_GENE  ASSOCIATED_REFSEQ_IDs`

Where a mouse gene has multiple human homologues they are output on separate lines.

humanmine.py requires python 3, and is run as follows (where <inputfile> is the list of mouse IDs):

`$ python3 humanmine.py <inputfile> > <outputfile>`

Results are writted to the <outputfile>

Not all mouse genes will identify a clear homologue in humanmine. To find these, run

`$ diff -y <(cut -f1 <inputfile>.genes) <inputfile> | grep ">"`

This will display the mouse IDs that are in the input but not the genes file. These can then be manually investigated using uniprot, BLAST, etc. to find a plausible homologue

### Making a BED file

The list of RefSeq IDs can be converted into a BED file for use in analysis. This is simple to do using the UCSC table browser:

https://genome.ucsc.edu/cgi-bin/hgTables

Settings used:
 * clade: Mammal
 * genome: Human
 * assembly: Feb. 2009 (GRCh37/hg19)
 * group: Genes and Gene Predictions
 * track: NCBI RefSeq
 * table: RefSeq all
 * region: genome
 * identifiers: paste list & copy in list of RefSeq IDs
 * output format: BED - browser extensible data
 * file type returned: plain text
 * Create one BED record per: Exons plus 100 bases at each end
  * NOTE: this setting includes UTR exons, but saves having to add flanking intron later

### Merging the BED file

Given that there are multiple transcripts for many of these genes, the BED file produced from UCSC table browser will contain many duplicated or very similar exons.
To avoid analysis problems we want to remove these duplicates, and ensure that every transcript is covered. To do this, we can use bedtools merge to merge overlapping
BED regions, while retaining the information about their transcript of origin.

`$ bedtools merge -c 4 -o collapse -i <bedfile> > <bedfile>.merged`

The BED file is now ready for use in further analysis.
